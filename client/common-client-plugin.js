const mapVideo = ({ currentTime, video }) => ({
  artist: (video.videoChannel || video.account).displayName,
  currentTime: currentTime || video.userHistory.currentTime,
  poster: video.previewUrl,
  title: video.name,
  url: video.streamingPlaylists
    ? video.streamingPlaylists[0].files.sort((a, b) => b.resolution.id - a.resolution.id).slice(0).pop().fileUrl
    : null,
  uuid: video.uuid,
});

let currentVideo;
let currentPlayer;
let currentPlaylist;
let currentPlaylistElements;
let currentPlaylistPosition;

window.peertubePlayerApiV2 = {
  getVideo: (uuid) =>
    fetch(`/api/v1/videos/${uuid}`, {
      headers: peertubeHelpers.getAuthHeader(),
    })
      .then(data => data.json())
      .then(data => mapVideo(data)),

  getPlayerStatus: () => new Promise(resolve => {
    const isPaused = !!currentPlayer && currentPlayer.paused();

    if (currentPlaylist) {
      resolve({
        isPaused,
        playlist: {
          currentPosition: currentPlaylistPosition,
          items: currentPlaylistElements[currentPlaylist.id]
            .filter(elem => elem.video)
            .map(elem => ({
              ...elem,
              video: mapVideo({
                video: elem.video.id === currentVideo.id
                  ? currentVideo
                  : elem.video,
              }),
            })),
          shortUUID: currentPlaylist.shortUUID,
          title: currentPlaylist.displayName,
        },
        singleVideo: null,
      });
    } else {
      resolve({
        isPaused,
        playlist: null,
        singleVideo: !currentPlayer
          ?  null
          : mapVideo({ currentTime: currentPlayer.currentTime(), video: currentVideo })
      })
    }
  })
,
  updatePlayerStatus: ({ currentTime, isPaused, playlistPosition }) => {
    if (playlistPosition && currentPlaylistPosition !== playlistPosition) {
      window.location.href = `/w/p/${currentPlaylist.shortUUID}?playlistPosition=${playlistPosition}&start=${currentTime}s&autoPlay=${!isPaused}`;
      return;
    }

    currentPlayer.currentTime(currentTime);

    if (isPaused) {
      currentPlayer.pause();
    } else {
      currentPlayer.play();
    }
  },
};

function register ({ registerHook, peertubeHelpers }) {
  registerHook({
    target: 'action:video-watch-playlist.elements.loaded',
    handler: ({ playlist, playlistElements }) => {
      currentPlaylistElements = {
        [playlist.id]: [...playlistElements],
      };
    },
  });

  registerHook({
    target: 'action:video-watch.player.loaded',
    handler: ({ player, playlist, playlistPosition, video }) => {
      currentVideo = video && { ...video };
      currentPlayer = player;
      currentPlaylist = playlist && { ...playlist };
      currentPlaylistPosition = playlistPosition;
    },
  });
}

export {
  register
}
