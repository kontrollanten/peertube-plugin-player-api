# PeerTube plugin player API

Creates an API so that the player API is reachable from outside PeerTube context.

**Isn't supported until https://github.com/Chocobozzz/PeerTube/pull/4387 is merged and released.**

## window.peertubePlayerApiV2.getPlayerStatus()
Returns a promise containing the following structure.

```
{
  isPaused: bool,
  playlist: { // playlist will be null when there's no playlist
    currentPosition: int,
    title: string,
    items: [
      {
        position: int,
        video: {
          artist: string,
          currentTime: float,
          poster: string, // URL to thumbnail
          title: string,
          url: string || null, // When null, the full video object can be retrieved by getVideo API.
        },
      },
    ],
  },
  singleVideo: { // singleVideo will be null when there's a playlist
    artist: string,
    currentTime: float,
    poster: string, // URL to thumbnail
    title: string,
    url: string,
  }
}
```

## window.peertubePlayerApiV2.getVideo(uuid)
Promise that returns the full video object.

```
{
  artist: string,
  currentTime: float,
  poster: string, // URL to thumbnail
  title: string,
  url: string,
}
```

## window.peertubePlayerApiV2.setPlayerStatus(options)
Takes the following option object.

```
{
  currentTime: float,
  isPaused: bool,
  playlistPosition: int, // should only be set when there's a playlist
}
```
